const menuItemCont = document.getElementById("menuItemCont");
var menuItems = null;
//const menuItemsLen = menuItems.getElementsByTagName('*').length;

var menuLength = 2;

var selectId = 0;

var titles = [];
var links = [];
var backgrounds = [];

function changeSel(left){
    if(left){
        prevId = selectId-1
        if(selectId-1 < 0){
            prevId = menuLength-1;
        }
        menuItems[selectId].classList.remove("active");
        menuItems[prevId].classList.add("active");
        selectId = prevId;
    }
    else{
        nextId = selectId+1
        if(selectId+1 > menuLength-1){
            nextId = 0;
        }
        menuItems[selectId].classList.remove("active");
        menuItems[nextId].classList.add("active");
        selectId = nextId;
    }

    updateTitle(selectId);

    menuItemCont.style.left = "-" + String(menuItemXMult * selectId) + "px";
}

function updateTitle(id){
    document.title = titles[id];
}

function clockUpdate(){
    function checkTime(i) {
        if (i < 10) i = "0" + i;
        return i;
    }

    var d = new Date();
    var h = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();

    var hh = checkTime(h);
    var mm = checkTime(m);
    var ss = checkTime(s);

    var dopt = { year: "numeric", month: "long", day: "numeric" };
    var dwopt = { weekday: 'long' };

    //var stzoff = 60 + d.getTimezoneOffset();
    //var stime = '@' + ('000'+Math.floor((h * 3600 + (m + stzoff) * 60 + s) / 86.4) % 1000).slice(-3);

    $('#clock #clockspan').text(`> ${hh+":"+mm+":"+ss}`);
    $('#clock #datespan').html(d.toLocaleString("pt-BR",dopt) + "<br>" + d.toLocaleString("pt-BR",dwopt));
}

function start(){
    document.title = "\uFEFF";

    $.getJSON("main.json", function(json) {
        menuLength = json.length;

        json.forEach(function(x) {
            var entry = '<div class="menuItem"><span class="icon"><svg data-src="'+x.icon+'"></svg></i></span><span class="bottom">'+x.name+'</span></div>';
            menuItemCont.innerHTML += entry;
            titles.push((x.name).toLowerCase());
            links.push(x.url);
            backgrounds.push(x.background);
        });
    }).done(() => {
        menuItems = document.querySelectorAll("#menuItems .menuItem");

        menuItemXMult = menuItems[0].offsetWidth + 12;

        menuItems[selectId].classList.add("active");

        updateTitle(selectId);
    });

    clockUpdate();
    setInterval(clockUpdate, 1000);

    $('#loading').fadeOut();
    $('#main').removeClass("invisible");
}

function xmasDetect(){
    const dateMonth = new Date().getMonth()+1;
    if(dateMonth == 12){
        xmas();
    }
}

function xmas(){
    document.documentElement.classList.add("xmas");
    $('#clock #specialspan').html("Feliz Natal!<br>Feliz "+(new Date().getFullYear()+1)+"!");
    $('#clock #specialspan').removeClass("hidden");
}

$(document).ready(() => {
    xmasDetect();
    start();
});

document.body.addEventListener('keydown', (e) =>{
    if(e.key == "r"){
        e.preventDefault();

        $('#main').addClass("invisible");
        $('#loading').fadeIn();
            
        document.title = "loading..."

        window.location.href = "https://rgmneocities.neocities.org/";
    }
    if(e.key == "x") {
        e.preventDefault();

        xmas();

        window.alert("Ativado modo Natal!");
    }
    if(e.key == "ArrowRight"){
        e.preventDefault();
        changeSel(false)
    }
    if(e.key == "ArrowLeft"){
        e.preventDefault();
        changeSel(true)
    }
    if(e.key == "Enter"){
        if(selectId <= menuLength){
            e.preventDefault();

            //$('#menuItems .menuItem:eq('+selectId+')').addClass("invisible");
            var notActiveItems = document.querySelectorAll("#menuItems .menuItem:not(.active)");
            notActiveItems.forEach((item) => {
                item.classList.add("not-select");
            })

            setTimeout(() => {
                $('#main').addClass("invisible");
                $('#loading').fadeIn();
            
                document.title = "loading..."

                window.location.href = links[selectId];
            },400);
        }
    }
});